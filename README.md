# DataAnon
Short description and motivation.

## Usage
Mount the route:
    config/routes.rb

    Rails.application.routes.draw do
       mount DataAnon::Engine, at: "/data_anon"
       ...
    end

### Basic Auth
If you need basic auth, set these two env variables:

    DATA_ANON_BASIC_AUTH_USER: "foo"
    DATA_ANON_BASIC_AUTH_PASS: "secret"

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'data_anon'
```

And then execute:
```bash
$ bundle
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
