module DataAnon
  class HomeController < ApplicationController
    before_action :basic_auth

    IGNORED_TABLES = ["ar_internal_metadata", "schema_migrations" ]
    IGNORED_COLUMNS = ["id", "created_at", "updated_at"]

    def index
      @tables = get_schema_information
      @strategies = ["scramble_data", "replace_email"]
    end

    private

    def basic_auth
      user = ENV['DATA_ANON_BASIC_AUTH_USER']
      password = ENV['DATA_ANON_BASIC_AUTH_PASS']
      return true unless user.present? && password.present?

      unless authenticate_with_http_basic { |u, p| u == user && p == password }
        request_http_basic_authentication
      end
    end

    def get_schema_information
      Rails.application.eager_load!

      data = []
      ActiveRecord::Base.connection.tables.each do |table|
        unless IGNORED_TABLES.include? table
          klass = get_class(table)

          if (klass)
            table_data = { table: klass.table_name, name: klass.name}
            columns = []
            klass.columns.each do |col|
              unless IGNORED_COLUMNS.include? col.name
                columns.push({ name: col.name, type: col.type, key: "#{table_data[:table]}-#{col.name}"})
              end
            end

            table_data[:columns] = columns
            data.push(table_data)
          end
        end
      end
      return data
    end



    def get_class(table_name)
      begin
        table_name.singularize.camelize.constantize
      rescue
        false
      end

    end
  end
end
