// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require_tree .


window.onload = function() {
  document.addEventListener("click", clickCallback)
  document.addEventListener("change", selectCallback)
  document.getElementById("reload").addEventListener("click", loadJSON);
};

var data = [];

const clickCallback = function(e) {
  var target, table, column, select, index;
  target = e.target;
  if (target.className !== "checkbox-input") return;
  table = target.dataset.table;
  column = target.dataset.column;


  console.log("click", table, column);

  if (target.type === "checkbox") {
    select = document.getElementById("select_" + table + "-" + column)
    if (target.checked) {
      select.disabled = false;
    } else {
      select.disabled = true;
      select.value = "";
      index = data.findIndex(function(i){ return i.table === table && i.column === column});
      data.splice(index, 1);
    }
  }
  updateOutput();
};


const selectCallback = function(e){
  var target, table, column, strategy, index, item;
  target = e.target;
  if (target.className !== "select-input") return;

  table = target.dataset.table;
  column = target.dataset.column;


  if (target.type === "select-one") {
    strategy = target.value;

    index = data.findIndex(function(i){return i.table === table && i.column === column});
    if (index !== -1) {
      item = data[index];
      item.strategy = strategy
    } else {
      data.push({table: table, column: column, strategy: strategy})
    }
  }
  updateOutput();
};

const updateOutput = function() {
  var el = document.getElementById("json-output");
  el.value = JSON.stringify(data);
};


const loadJSON = function() {

  var el = document.getElementById("json-output");
  try {
    data = JSON.parse(el.value);

    // First reset all fields
    var checkboxes = document.getElementsByClassName("checkbox-input")
    var i, checkbox, table, column, select;

    for (i=0; i < checkboxes.length; i++) {
      checkbox = checkboxes[i];

      table = checkbox.dataset.table;
      column = checkbox.dataset.column;

      select = document.getElementById("select_" + table + "-" + column);
      checkbox.checked = false;
      select.value = null;
      select.disabled = true;
    }

    // Enable the fields configured
    for (i = 0; i < data.length; i++) {
      var d = data[i];
      checkbox = document.getElementById("checkbox_" + d.table + "-" + d.column);
      select = document.getElementById("select_" + d.table + "-" + d.column);
      checkbox.checked = true;
      select.disabled = false;
      select.value = d.strategy;

    }
  }
  catch(err) {
    data = [];
    window.alert("Error: " + err);
  }
};
