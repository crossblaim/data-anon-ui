$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "data_anon/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "data_anon"
  s.version     = DataAnon::VERSION
  s.authors     = ["Eloy Gomez"]
  s.email       = ["eloy@indeos.es"]
  # s.homepage    = "TODO"
  s.summary     = " Data Anon"
  s.description = "DataAnon"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 4.0"
end
